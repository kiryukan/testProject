<title>Inscription</title>
<div>
    <br><legend><h2>Inscription</h2></legend><br>
</div>
<div class="container col-lg-10 col-lg-offset-4">
	<form action='#' method='post' name="inscriptionForm">
		<div class="form-group">
			<label for="nom">Nom :</label>
			  <input type="text" class="form-control" id="nom" name="nom" required="true" placeholder="Entrez votre nom"
			  pattern="[A-Za-z]{3,20}" title="Entrez minimum trois lettres de votre nom (aucun caractere speciaux ni chiffre est valide)" >
			  <p id="demo"></p>
        </div>
        <div class="form-group">
			  <label for="prenom" id="color_w">Prenom :</label>
			  <input type="text" class="form-control" id="prenom" name="prenom" required="true" placeholder="Entrez votre prenom"
			  	pattern="[A-Za-z]{3,20}" title="Three letter country code">
        </div>
        <div class="form-group">
			  <label for="mail">E-Mail:</label>
			  <input type="text" class="form-control" id="mail" name="mail" required="true" placeholder="Entrez votre E-Mail">
        </div>
        <div class="form-group">
			  <label for="userName">Username:</label>
			  <input type="text" class="form-control" id="userName" name="userName" required="true" placeholder="Entrez votre username"
			  	pattern="[A-Za-z]{1,20}" title="Three letter country code">
        </div>
        <div class="form-group">
			  <label for="pwd">Password:</label>
			  <input type="password" class="form-control" id="pwd" name="pwd" required="true" placeholder="Entrez votre password">
        </div>
        <div class="col-lg-offset-5">
            <input type="submit" name="button" value="Enregistrer" class="btn btn-default" />
        </div>
	 </form>
</div>
<?php include_once 'connexionDB.php'; ?>
<?php
$n =$p = $e=$u=$p ="";
if (isset($_POST['nom'])) { $n = $_POST['nom']; }
if (isset($_POST['prenom'])) { $pr = $_POST['prenom']; }
if (isset($_POST['mail'])) { $e = $_POST['mail']; }
if (isset($_POST['userName'])) { $u = $_POST['userName']; }
if (isset($_POST['pwd'])) { $p = $_POST['pwd']; }
if(isset($_POST['button'])){
$sql = "INSERT INTO user (nom, prenom, email, username, password)
VALUES ('$n','$pr','$e','$u','$p')";
if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
}
$sql2 = "CREATE TRIGGER roleTrigger AFTER INSERT ON user
    FOR EACH ROW BEGIN
        INSERT INTO user_role (user_role_id, user_role, userId) VALUES (,,user.userId);
    END";
$conn->query($sql2);
$conn->close();
?>