<?php include_once 'connexionDB.php'; ?>
<?php
$sql = "SELECT * FROM user";
$result= $conn->query($sql);
if ($conn->query($sql) == TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

?>
<title>Liste des clients</title>
<h1>Liste des Clients</h1>
<div class="container">
	<div class="table-responsive">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Nom</th>
					<th>Prenom</th>
					<th>E-mail</th>
                    <th>Username</th>
				</tr>
			</thead>
			<tbody>
            <tr>
            <?php
                while($row = mysqli_fetch_array($result)){
                    echo "<td>" . $row['userId'] . "</td>";
                    echo "<td>" . $row['nom'] . "</td>";
                    echo "<td>" . $row['prenom'] . "</td>";
                    echo "<td>" . $row['email'] . "</td>";
                    echo "<td>" . $row['username'] . "</td>";

                }
            $conn->close();
            ?>
                <td> <button type="button" class="btn btn-primary">Modifier</button>
                    <button type="button" class="btn btn-primary" >Suprimer</button></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

