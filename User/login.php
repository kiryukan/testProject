<title>Login</title>
<?php
/**
 * Created by PhpStorm.
 * User: E
 * Date: 14-03-18
 * Time: 15:33
 */

?>
<div>
    <legend><h2>Login</h2></legend><br>
</div>
<div class="container col-lg-10 col-lg-offset-4">
    <form action='#' method='post' name="login">
        <div class="form-group">
            <label for="userName">Username:</label>
            <input type="text" class="form-control" id="userName" name="userName" required="true" placeholder="Entrez votre username"
                   pattern="[A-Za-z]{1,20}" title="Three letter country code">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="pwd" required="true" placeholder="Entrez votre password">
        </div>
        <div class="col-lg-offset-5">
        <input type="submit" name="button" value="Login" class="btn btn-default col-lg-2" />
        </div>
    </form>
</div>
