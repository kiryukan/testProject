<title>Ajouter concert</title>
<div>
    <legend><h2>Ajouter concert</h2></legend><br>
</div>
<div class="container col-lg-10 col-lg-offset-4">
    <form action='#' method='post' name="inscriptionForm">
        <div class="form-group">
            <label for="nomConcert">Nom du concert :</label>
            <input type="text" class="form-control" id="nomConcert" name="nomConcert" required="true" placeholder="Entrez le nom du concert"
                   pattern="[A-Za-z]{3,20}" title="Entrez minimum trois lettres du nom du concert. (aucun caractere speciaux ni chiffre est valide)" >
            <p id="demo"></p>
        </div>
        <div class="form-group">
            <label for="nbPlaces">Nombres des places :</label>
            <input type="text" class="form-control" id="nbPlaces" name="nbPlaces" required="true" placeholder="Entrez un nombre de places"
                   pattern="[0-9]{2,20}" title="Three letter country code">
        </div>
        <div class="form-group">
            <label for="nbArtistes">Nombre d'artistes :</label>
            <input type="text" class="form-control" id="nbArtistes" name="nbArtistes" required="true" placeholder="Entrez un nombre"
                   pattern="[0-9]{2,20}" title="Three letter country code">
        </div>
        <div class="form-group">
            <label for="dateConcert">Date Concert:</label>
            <input type="date" class="form-control" id="dateConcert" name="dateConcert" required="true" placeholder="Entrez la date du concert">
        </div>
        <button type="submit" name="button" class="btn btn-default">Enregistrer</button>
    </form>
</div>
<?php include_once 'connexionDB.php'; ?>
<?php
$n =$p = $e=$u=$p ="";
if (isset($_POST['nomConcert'])) { $n = $_POST['nomConcert']; }
if (isset($_POST['nbPlaces'])) { $pr = $_POST['nbPlaces']; }
if (isset($_POST['nbArtistes'])) { $e = $_POST['nbArtistes']; }
if (isset($_POST['dateConcert'])) { $u = $_POST['dateConcert']; }
if(isset($_POST['button'])){
$sql = "INSERT INTO concert (nomConcert, nbrePlace, nbreArtistes, dateConcert)
VALUES ('$n','$pr','$e','$u')";

if ($conn->query($sql) === TRUE) {
    echo "New record created successfully";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}
}
$conn->close();
?>