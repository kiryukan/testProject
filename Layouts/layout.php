<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">

	<!-- LIBRERIAS BOOTSTRAP-->

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/css/style.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="/bootstrap/js/bootstrap.min.js"></script>
</head>
<body id="body">
<header>
	<?php 
		require_once('header.php');
	?>	
</header>

<section>	
	<div class="container">
	<?php
			require_once('routing.php');
	 ?>

	</div>
</section>

<footer>
	<?php 
		include_once('footer.php');
	?>
</footer>
</body>
</html>