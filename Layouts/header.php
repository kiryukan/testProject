<img src="/resources/images/concert.jpg" class="rounded" width="500" height="250" class="float-left" alt="Cinque Terre">

<nav class="navbar navbar-inverse nav col-lg-10 col-lg-offset-1">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand">Concert's Ecode</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="?menu=index">Home</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Gestion des concerts <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="?menu=ajoutConcert">Ajouter concert</a></li>
          <li><a href="?menu=ajoutTicket">Ajouter ticket</a></li>
          <li><a href="?menu=listeUser">Liste client</a></li>
        </ul>
      </li>
      <li><a href="#">Page 2</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="?menu=inscription"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="?menu=login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>