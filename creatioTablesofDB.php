<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "concert";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// sql to create table
$sql = "CREATE TABLE IF NOT EXISTS user (
userId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
nom VARCHAR(30) NOT NULL,
prenom VARCHAR(30) NOT NULL,
email VARCHAR(50) NOT NULL,
username VARCHAR(30) NOT NULL,
password VARCHAR(20) NOT NULL,
enabled INT (10) DEFAULT '1',
reg_date TIMESTAMP)";
if ($conn->query($sql) == TRUE) {
    echo "Table user created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS concert (
concertId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
nomConcert VARCHAR(30) NOT NULL,
nbrePlace INT(6) UNSIGNED NOT NULL,
nbreArtistes INT(6) UNSIGNED NOT NULL,
dateconcert DATE NOT NULL
)";

if ($conn->query($sql) == TRUE) {
    echo "Table user_role created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS ticket(
ticketId INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
userId INT(6) UNSIGNED NOT NULL,
prix FLOAT NOT NULL)";
if ($conn->query($sql) == TRUE) {
    echo "Table ticket created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
$sql = "CREATE TABLE IF NOT EXISTS user_role (
user_role_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
role VARCHAR(30) DEFAULT 'ROLE_USER',
userId INT(6) UNSIGNED NOT NULL)";

if ($conn->query($sql) == TRUE) {
    echo "Table user_role created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}


$conn->close();
?>